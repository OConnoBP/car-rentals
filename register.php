<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
        <title>Enterprise - Register</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <?php
            session_start();
            if($_SERVER["REQUEST_METHOD"] == "POST"){
                include('db_utility.php');
                $first_name_error = "";
                $last_name_error = "";

                $firstname = strval(@$_POST['fname']);
                $lastname = strval(@$_POST['lname']);
                $email = strval(@$_POST['email']);
                $username = strval(@$_POST['username']);
                $password = strval(@$_POST['password']);
                $passwordconfirm = strval(@$_POST['passwordconfirm']);
                $submit = boolval(@$_POST['submit']);
                $encpassword = md5($password);

                $error_text = "";

                $isValid = TRUE;

                if($firstname == null || strlen($firstname) > 18){
                    $error_text = $error_text . "*Firstname cannot be blank and must be less than 19 characters<br>";
                    $isValid = FALSE;
                }
                if($lastname == null || strlen($lastname) > 18){
                    $error_text = $error_text . "*Lastname cannot be blank and must be less than 19 characters<br>";
                    $isValid = FALSE;
                }
                if($email == null || !filter_var($email, FILTER_VALIDATE_EMAIL)){
                    $error_text = $error_text . "*Email is invalid<br>";
                    $isValid = FALSE;
                }
                if($username == null || strlen($username) > 50){
                    $error_text = $error_text . "*Username is invalid<br>";
                    $isValid = FALSE;
                }
                $sql = "SELECT username FROM users WHERE username = ?;";
                $valid_username = TRUE;
                if($stmt = $db->prepare($sql)){
                    $stmt->bind_param('s', $username);
                    $stmt->bind_result($duplicate_user_names);
                    $stmt->execute();
                    while($stmt->fetch()){
                        if($duplicate_user_names == $username){
                            $valid_username = FALSE;
                        }
                    }
                }
                if($valid_username == FALSE){
                    $error_text = $error_text . "*Username is already taken<br>";
                    $isValid = FALSE;
                }
                $stmt->close();
                if($password == null){
                    $error_text = $error_text . "*You must enter a password<br>";
                    $isValid = FALSE;
                }
                if($passwordconfirm == null){
                    $error_text = $error_text . "*You must confirm your password<br>";
                    $isValid = FALSE;
                }
                if($passwordconfirm != $password){
                    $error_text = $error_text . "*Password does not match password confirmation<br>";
                    $isValid = FALSE;
                }
                if(strlen($password) < 6){
                    $error_text = $error_text . "*Password must be at least 6 characters in length<br>";
                    $isValid = FALSE;
                }
                if($isValid == TRUE){
                    $sql = "INSERT INTO users(firstname, lastname, email, username, password) VALUES(?, ?, ?, ?, ?);";
                    if($stmt = $db->prepare($sql)){
                        $stmt->bind_param('sssss', $firstname, $lastname, $email, $username, $encpassword);
                        $stmt->execute();
                        $stmt->close();
                    }
                    $sql = "SELECT LAST_INSERT_ID();";
                    if($stmt = $db->prepare($sql)){
                        $stmt->bind_result($uid);
                        $stmt->execute();
                        if($stmt->fetch()){
                            $_SESSION["userid"] = $uid;
                        }
                        $stmt->close();
                    }

                    $_SESSION["username"] = $username;
                    header('Location: index.php');
                    exit();
                }
            }
        ?>
    </head>
    <body style="background: #EC9C57">
        <div class="container row valign-wrapper login-box center-align">
            <div class="col s12 l6 offset-l3 valign">
                <div class="card black-text">
                    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                        <div class="card-content black-text">
                            <img class="responsive-img" src="images/logo.png"/>
                            <span class="card-title black-text">Register</span>
                            <div class="row valign-wrapper">
                                <div class="col s6">
                                    <div class="input-field">
                                        <i class="material-icons prefix">face</i>
                                        <input id="fname" type="text" name="fname" value="<?php echo(@$firstname) ?>"/>
                                        <label for="fname">First Name</label>
                                    </div>
                                </div>
                                <div class="col s5">
                                    <div class="input-field">
                                        <input id="lname" type="text" name="lname" value="<?php echo(@$lastname) ?>"/>
                                        <label for="lname">Last Name</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row valign-wrapper">
                                <div class="col s11">
                                    <div class="input-field">
                                        <i class="material-icons prefix">email</i>
                                        <input id="email" type="text" name="email" value="<?php echo(@$email) ?>"/>
                                        <label for="email">Email</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row valign-wrapper">
                                <div class="col s11">
                                    <div class="input-field">
                                        <i class="material-icons prefix">account_circle</i>
                                        <input id="username" type="text" name="username" value="<?php echo(@$username) ?>"/>
                                        <label for="username">Username</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row valign-wrapper">
                                <div class="col s11">
                                    <div class="input-field">
                                        <i class="material-icons prefix">lock</i>
                                        <input id="password" type="password" name="password"/>
                                        <label for="password">Password</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row valign-wrapper">
                                <div class="col s11">
                                    <div class="input-field">
                                        <i class="material-icons prefix">lock</i>
                                        <input id="passwordconfirm" type="password" name="passwordconfirm"/>
                                        <label for="passwordconfirm">Confirm Password</label>
                                    </div>
                                </div>
                            </div>
                            <span class="error-text"><?php echo(@$error_text) ?></span>
                        </div>
                        <div class="card-action center">
                            <button class="green darken-1 btn-flat white-text right form-button" type="submit" name="submit"><strong>Register Now</strong></button><br><br>
                            <span style="text-align:center;">Already have an account? <a href="login.php">Login</a></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="js/materialize.js"></script>
        <script src="js/init.js"></script>
    </body>
</html>