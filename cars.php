
<?php
$servername = "localhost";
$username = "user";
$password = "";
$dbname = "carrental";

$conn = new mysqli($servername, $username, $password, $dbname);

if($conn->connect_error){
	die("Connection failed: " . $conn->connect_error);
}

$id_carType = @$_GET['carType'];
if($id_carType == null){
	echo "<tr>";
	echo "<td>";
	echo "No car type selected";
	echo "</td>";
	echo "<td>";
	echo "";
	echo "</td>";
	echo "<td>";
	echo "";
	echo "</td>";
	echo "<td>";
	echo "";
	echo "</td>";
	echo "</tr>";
}
else{
	$sql = "SELECT CarImage, VIN, Mileage FROM car WHERE Type = " . $id_carType . " AND Rented = 0;";
	$result = $conn->query($sql);
	
	while($row = $result->fetch_assoc()){
		if(@$_POST['car'] == $row["VIN"]){
			echo "<tr>";
			echo "<td>";
			echo "<p><input id='" . $row["VIN"] . "' type='radio' value='" . $row["VIN"] . "' name='car' checked/><label for='" . $row["VIN"] . "'></label></p>";
			echo "</td>";
			echo "<td>";
			echo $row["CarImage"];
			echo "</td>";
			echo "<td>";
			echo $row["VIN"];
			echo "</td>";
			echo "<td>";
			echo $row["Mileage"];
			echo "</td>";
			echo "</tr>";
		}
		else{
			echo "<tr>";
			echo "<td>";
			echo "<p><input id='" . $row["VIN"] . "' type='radio' value='" . $row["VIN"] . "' name='car' /><label for='" . $row["VIN"] . "'></label></p>";
			echo "</td>";
			echo "<td>";
			echo "<img src='" . $row["CarImage"] . "' class='responsive-img'/>";
			echo "</td>";
			echo "<td>";
			echo $row["VIN"];
			echo "</td>";
			echo "<td>";
			echo $row["Mileage"];
			echo "</td>";
			echo "</tr>";
		}
		
	}
	
	$conn->close();
}

?>