<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
        <title>Enterprise - Login</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <?php
        // db_utility contains code for connecting to database
        include('db_utility.php');

        // if the user is already logged in, go to index.php
        if(isset($_SESSION['username'])){
            header('Location: index.php');
            exit();
        }
        else{
            $username = strval(@$_POST['username']);
            $password = strval(@$_POST['password']);
            $submit = boolval(@$_POST['submit']);
            // encrypts the user's entered password
            $encpassword = md5($password);
            if($_SERVER["REQUEST_METHOD"] == "POST"){
                $sql = "SELECT id, username FROM users WHERE username = ? AND password = ?;";
                if($stmt = $db->prepare($sql)){
                    $stmt->bind_param('ss', $username, $encpassword);
                    $stmt->bind_result($_SESSION["userid"], $_SESSION['username']);
                    $stmt->execute();
                    $stmt->fetch();
                    $stmt->close();
                    // if user exists and passwords match, redirect to index.php
                    if($_SESSION["userid"] != NULL && $_SESSION["username"] != NULL){
                        header('Location: index.php');
                        exit();
                    }
                    else{
                        $login_error = "*Invalid username and/or password";
                    }
                }
            }
        }
        ?>
    </head>
    <body>
        <div class="container row valign-wrapper login-box center-align">
            <div class="col s12 l6 offset-l3 valign card">
                <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
                    <div class="card-content black-text">
                        <img class="responsive-img" src="images/logo.png"/>
                        <span class="card-title black-text">Login</span>
                        <div class="row valign-wrapper">
                            <div class="col s11">
                                <div class="input-field">
                                    <i class="material-icons prefix">account_circle</i>
                                    <input type="text" name="username" id="username" class="validate" value="<?php echo(@$username) ?>"/><br>
                                    <label for="username">Username</label>
                                </div>
                            </div>
                        </div>
                        <div class="row valign-wrapper">
                            <div class="col s11">
                                <div class="input-field">
                                    <i class="material-icons prefix">lock</i>
                                    <input type="password" name="password" id="password" class="validate"/>
                                    <label for="password">Password</label>
                                </div>
                            </div>
                        </div>
                        <span class="error-text"><?php echo(@$login_error) ?></span>    
                    </div>
                    <div class="card-action center">
                        <button class="green darken-1 btn-flat white-text right form-button" type="submit" name="submit"><strong>Login</strong></button><br><br>
                        <span>Not a user? </span><a href="register.php">Register</a><br>
                    </div>
                </form>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="js/materialize.js"></script>
        <script src="js/init.js"></script>
    </body>
</html>