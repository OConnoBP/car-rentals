var cost = 0;
var carTypeCost = 0;
var rentalDays = 1;
function getCarType(carType){
    if(carType == 1){
        carTypeCost = 19.95;
    }
    else if(carType == 2){
        carTypeCost = 24.95;
    }
    else if(carType == 3){
        carTypeCost = 39.00;
    }
    console.log(carType);
    getCars(carType);
    addCost();
}

function getCars(carType){
    var xmlhttp;

    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }
    else{
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function(){
        if(this.readyState == 4 && this.status == 200){
            document.getElementById("table-body").innerHTML = this.responseText;
        }
    };
    xmlhttp.open("GET", "cars.php?carType=" + carType, true);
    xmlhttp.send();
}

function addCost(){
    cost = carTypeCost * rentalDays;
    cost = (Math.round(100 * cost) / 100).toFixed(2);
    cost = "$" + cost;
    document.getElementById("preliminaryRentalCost").innerHTML = cost;
}

function setRentalDays(days){
    this.rentalDays = days;
    addCost();
}