/*$( document ).ready(function() {
  $('.carousel').carousel();
  $('.button-collapse').sideNav();
  $('.dropdown-button').dropdown();
  $('.collapsible').collapsible();
  $('.carousel.carousel-slider').carousel({fullWidth: true});
});*/

(function ($) {
  $(function () {
    $('.carousel').carousel();
    $('.button-collapse').sideNav();
    $('.dropdown-button').dropdown();
    $('.collapsible').collapsible();
    $('.carousel.carousel-slider').carousel({fullWidth: true});
  }); // end of document ready
})(jQuery); // end of jQuery name space