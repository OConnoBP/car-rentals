<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
        <title>Enterprise - Rented Cars</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    </head>
    <body>
        <?php
        include('db_utility.php');
        if($_SESSION['username'] == null){
            header('Location: login.php');
            exit();
        }
        ?>
        <?php include('navbar.php'); ?>
        <div class="container">
		    <div class="card rental-form">
			    <div class="card-content">
				    <ul class="collection">
					    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
      					    <li class="collection-item">
							    <h5 class="center">Rented Car</h5>
                                <table class="table table-striped">
                                    <thead>
                                        <th></th>
                                        <th>VIN</th>
                                        <th>Type</th>
                                        <th>Mileage</th>
                                        <th>Days</th>
                                    </thead>
                                    <tbody id="table-body">
                                    <?php
                                        mysqli_report(MYSQLI_REPORT_ERROR);

                                        $a_rental_id;
                                        $sql = "SELECT ID FROM rental INNER JOIN car ON rental.VIN = car.VIN WHERE rental.UserId = ? AND car.Rented = 1;";
                                        if($stmt = $db->prepare($sql)){
                                            $stmt->bind_param('i', $_SESSION['userid']);
                                            $stmt->bind_result($a_rental_id);
                                            $stmt->execute();
                                            $stmt->fetch();
                                            $stmt->close();
                                        }

                                        if($_SERVER["REQUEST_METHOD"] == "POST"){
                                            $rental_mileage = @$_POST['mileage'];
                                            $rental_id = @$_POST['rentalId'];
                                            $final_mileage;
                                            $final_charge = $rental_mileage * 0.32;
                                            $_vin;
                                            $old_charge;
                                            $final_final_charge;
                                            $error_text = "";
                                            if($rental_id != null){
                                                $is_valid = true;
                                                if($rental_mileage > 500000 || $rental_mileage < 0){
                                                    $is_valid = false;
                                                    $error_text = $error_text . "*Invalid mileage<br>";
                                                }
    
                                                if($is_valid){
                                                    // Retrieve the VIN of the car that is currently rented by the user
                                                    $sql = "SELECT car.`VIN` FROM `car` INNER JOIN `rental` ON car.`VIN` = rental.`VIN` WHERE rental.`UserId` = ? AND car.`Rented` = 1;";
                                                    if($stmt = $db->prepare($sql)){
                                                        $stmt->bind_param('i', $_SESSION['userid']);
                                                        $stmt->bind_result($vin);
                                                        $stmt->execute();
                                                        if($stmt->fetch()){
                                                            $_vin = $vin;
                                                        }
                                                        $stmt->close();
                                                    }
                                                
                                                    // Retrieve the mileage of the car before it was rented
                                                    $sql = "SELECT `Mileage` FROM `car` WHERE `VIN` = ?;";
                                                    if($stmt = $db->prepare($sql)){
                                                        $stmt->bind_param('s', $_vin);
                                                        $stmt->bind_result($old_mileage);
                                                        $stmt->execute();
                                                        while($stmt->fetch()){
                                                            $final_mileage = $rental_mileage + $old_mileage;
                                                        }
                                                        $stmt->close();
                                                    }
                                                
                                                    // Update car mileage and rented status
                                                    $sql = "UPDATE car SET Mileage = " . $final_mileage . ", Rented = 0 WHERE VIN = '" . $_vin . "';";
                                                    if($stmt = $db->prepare($sql)){
                                                        $stmt->execute();
                                                    }
                                                    $stmt->close();
    
                                                    // Retrieve the original charge and calculate the final charge
                                                    $sql = "SELECT Charge FROM rental WHERE ID = " . $a_rental_id . ";";
                                                    if($stmt = $db->prepare($sql)){
                                                        $stmt->bind_result($an_old_charge);
                                                        $stmt->execute();
                                                        if($stmt->fetch()){
                                                            $final_final_charge = $an_old_charge + $final_charge;
                                                        }
                                                        $stmt->close();
                                                    }
                                                
                                                    // select the charge from rental where VIN = _vin and UserId = userid and rented = 1
                                                    // update rental miles and charge
                                                    $sql = "UPDATE rental SET Miles = ?, Charge = " . $final_final_charge . " WHERE ID = ?;";
                                                    if($stmt = $db->prepare($sql)){
                                                        $stmt->bind_param('ii', $rental_mileage, $rental_id);
                                                        $stmt->execute();
                                                        $stmt->close();
                                                    }
                                                }
                                            }
                                            
                                        }

                                        $initial_charge;
                                        
                                        $sql = "SELECT car.`CarImage`, car.`VIN`, cartype.`Name`, car.`Mileage`, rental.`Days` FROM `car` INNER JOIN `rental` ON car.`VIN` = rental.`VIN` INNER JOIN cartype ON car.`Type` = cartype.`ID` WHERE rental.`UserId` = ? AND car.`Rented` = 1 ORDER BY rental.`ID` DESC LIMIT 1;"; //and car rented = 1
                                        if($stmt = $db->prepare($sql)){
                                            $stmt->bind_param('i', $_SESSION['userid']);
                                            $stmt->bind_result($car_image, $vin, $type, $mileage, $days);
                                            $stmt->execute();
                                            if($stmt->fetch()){
                                                echo "<tr>";
                                                echo "<td>";
                                                echo "<img src='" . $car_image . "' class='responsive-img'/>";
                                                echo "</td>";
                                                echo "<td>";
                                                echo $vin;
                                                echo "</td>";
                                                echo "<td>";
                                                echo $type;
                                                echo "</td>";
                                                echo "<td>";
                                                echo $mileage;
                                                echo "</td>";
                                                echo "<td>";
                                                echo $days;
                                                echo "</td>";
                                                echo "</tr>";
                                            }
                                            else{
                                                echo "<tr>";
                                                echo "<td>";
                                                echo "No cars rented";
                                                echo "</td>";
                                                echo "<td>";
                                                echo "</td>";
                                                echo "<td>";
                                                echo "</td>";
                                                echo "<td>";
                                                echo "</td>";
                                                echo "<td>";
                                                echo "</td>";
                                                echo "</tr>";
                                            }
                                        }
                                         
                                        function get_initial_cost(){
                                            include('db_utility.php');
                                            $sql = "SELECT Charge FROM rental WHERE ID = " . get_rental_id() . ";";
                                            if($stmt = $db->prepare($sql)){
                                                $stmt->bind_result($charge);
                                                $stmt->execute();
                                                if($stmt->fetch()){
                                                    echo $charge;
                                                }
                                            }
                                        }
                                        
                                        function get_rental_id(){
                                            include('db_utility.php');
                                            $sql = "SELECT ID FROM rental INNER JOIN car ON rental.VIN = car.VIN WHERE rental.UserId = ? AND car.Rented = 1 ORDER BY ID DESC LIMIT 1;";
                                            if($stmt = $db->prepare($sql)){
                                                $stmt->bind_param('i', $_SESSION['userid']);
                                                $stmt->bind_result($rental_id);
                                                $stmt->execute();
                                                if($stmt->fetch()){
                                                    return $rental_id;
                                                }
                                            }
                                        }

                                        function get_rental_id_for_form(){
                                            echo get_rental_id();
                                        }
                                    ?>
                                    </tbody>
                                </table>
                            </li>
                            <li class="collection-item">
                                <h5 class="center">Return Car</h5>
                                <div class="input-field">
                                    <input name="mileage" id="mileage" type="number" onkeyup="add_mileage_to_cost(this.value)"/>
                                    <label for="mileage">Mileage</label>
                                </div>
                            </li>
                            <li class="collection-item">
                                <h5 class="center">Total Cost</h5>
							    <h4 id="totalRentalCost" class="center">
                                <?php 
                                    if(get_rental_id() != null){
                                        get_initial_cost();
                                    } 
                                    else{
                                        echo '$0.00';
                                    }
                                ?>
                                </h4>
                            </li>
                            <input type="hidden" name="rentalId" value="<?php get_rental_id_for_form() ?>"/>
						    <li class="collection-item">
							    <span class="error-text"><?php echo(@$error_text); ?></span>
							    <button class="green darken-1 btn-flat white-text form-button" type="submit" name="submit"><strong>Return Car</strong></button><br>
						    </li>
					    </form>
    			    </ul>
			    </div>
		    </div>
	    </div>
	    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="js/materialize.js"></script>
        <script src="js/init.js"></script>
        <script>
            var oldCost = parseFloat(document.getElementById('totalRentalCost').innerHTML);

            function add_mileage_to_cost(mileage){
                if(mileage >= 0 && mileage < 500000){
                    var mileagecost = mileage * 0.32;
                    var newcost = oldCost + mileagecost;
                    newcost = Math.round((100*newcost))/100;
                    newcost = newcost.toFixed(2);
                    document.getElementById('totalRentalCost').innerHTML = newcost;
                }
            }
        </script>
    </body>
</html>