<?php session_start(); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
        <title>Enterprise - Rent a Car</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script>
			var cost = 0;
			var carTypeCost = 0;
			var rentalDays = 1;
			function getCarType(carType){
				if(carType == 1){
					carTypeCost = 19.95;
				}
				else if(carType == 2){
					carTypeCost = 24.95;
				}
				else if(carType == 3){
					carTypeCost = 39.00;
				}
				getCars(carType);
				addCost();
			}

			function getCars(carType){
				var xmlhttp;

				if(window.XMLHttpRequest){
					xmlhttp = new XMLHttpRequest();
				}
				else{
					xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
				}

				xmlhttp.onreadystatechange = function(){
					if(this.readyState == 4 && this.status == 200){
						document.getElementById("table-body").innerHTML = this.responseText;
					}
				};
				xmlhttp.open("GET", "cars.php?carType=" + carType, true);
				xmlhttp.send();
			}

			function addCost(){
				cost = carTypeCost * rentalDays;
				cost = (Math.round(100 * cost) / 100).toFixed(2);
				cost = "$" + cost;
				document.getElementById("preliminaryRentalCost").innerHTML = cost;
			}

			function setRentalDays(days){
				this.rentalDays = days;
				addCost();
			}
		</script>
	</head>
	<body>
	<?php
		include('db_utility.php');
		if($_SESSION['username'] == null){
			header('Location: login.php');
			exit();
		}
		else{
			if($_SERVER["REQUEST_METHOD"] == "POST"){
				$car_type_id = @$_POST["carType"];
				$car_id = @$_POST["car"];
				$rental_days = @$_POST["rentalDays"];
				// The price of the car being rented
				$car_price = 0.00;
				// The initial price of the rental, car_price * days
				$rental_price = 0.00;
				// Determines if the form will be submitted based on input requirements
				$isValid = TRUE;
				$error_text = "";

				if($car_type_id == NULL){
					$error_text = $error_text . "*You must enter a car type<br>";
					$isValid = FALSE;
				}

				if($car_id == NULL){
					$error_text = $error_text . "*You must select a car<br>";
					$isValid = FALSE;
				}

				if(($rental_days < 1 || $rental_days > 30) || $rental_days == NULL){
					$error_text = $error_text . "*Invalid rental duration (Pick between 0-30 days)<br>";
					$isValid = FALSE;
				}

				$already_renting_car = FALSE;
				// Determines if the user is already rented a car
				$sql = "SELECT car.Rented FROM car INNER JOIN rental ON car.VIN = rental.VIN INNER JOIN users ON rental.UserId = users.id WHERE car.Rented = 1 AND users.id = ?;";
				if($stmt = $db->prepare($sql)){
					$stmt->bind_param('i', $_SESSION['userid']);
					$stmt->bind_result($number_of_rentals);
					$stmt->execute();
					if($stmt->fetch()){
						$already_renting_car = TRUE;
					}
				}

				if($already_renting_car == TRUE){
					$error_text = $error_text . "*You are already renting a car. Please return that car before starting another rental<br>";
					$isValid = FALSE;
				}

				if($isValid == TRUE){
					// Gets the cost of the rental based on the car type
					$sql = "SELECT RentalCost FROM cartype WHERE ID = ?;";
					if($stmt = $db->prepare($sql)){
						$stmt->bind_param('i', $car_type_id);
						$stmt->bind_result($a_car_price);
						$stmt->execute();
						if($stmt->fetch()){
							$car_price = $a_car_price;
						}
						$stmt->close();
					}
					
					$rental_price = $car_price * $rental_days;

					// Records the rental in the database
					$sql = "INSERT INTO rental(VIN, UserId, Miles, Days, Charge) VALUES(?, ?, ?, ?, ?);";
					if($stmt = $db->prepare($sql)){
						$stmt->bind_param('sidid', $car_id, $_SESSION['userid'], $a = 0, $rental_days, $rental_price);
						$stmt->execute();
						$stmt->close();
					}
					
					// Updates the rental status of the car
					$sql = "UPDATE car SET Rented = 1 WHERE VIN = ?;";
					if($stmt = $db->prepare($sql)){
						$stmt->bind_param('s', $car_id);
						$stmt->execute();
						$stmt->close();
					}
					header('Location: rentedcars.php');
					exit();
				}
			}
		}

		// Retrieves the car types from the database
		function get_car_types(){
			include('db_utility.php');
			$sql = "SELECT ID, Name, RentalCost FROM cartype;";
			$result = $db->query($sql);
			while($row = $result->fetch_assoc()){
				echo "<p>";
				if(@$_POST["carType"] == $row["ID"]){
					echo "<input id='" . $row["Name"] . "' type='radio' value='" . $row["ID"] . "' name='carType' onchange='getCarType(this.value)' checked/>";
					echo "<label for='" . $row["Name"] . "'>" . $row["Name"] . " ($" . $row["RentalCost"] . "/day)</label>";
				}
				else{
					echo "<input id='" . $row["Name"] . "' type='radio' value='" . $row["ID"] . "' name='carType' onchange='getCarType(this.value)'/>";
					echo "<label for='" . $row["Name"] . "'>" . $row["Name"] . " ($" . $row["RentalCost"] . "/day)</label>";
				}
				
				echo "</p>";
			}
		}
	?>
	<?php include('navbar.php'); ?>
	<div class="container">
		<div class="card rental-form">
			<div class="card-content">
				<ul class="collection">
					<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
      					<li class="collection-item">
							<h5 class="center">Car Types</h5>
							<div id="carTypes"><?php get_car_types(); ?></div>
						</li>
      					<li class="collection-item">
							<h5 class="center">Choose Car</h5>
							<table class="table striped">
							  	<thead>
									<th>Select a Car</th>
									<th></th>
									<th>VIN</th>
									<th>Mileage</th>
								</thead>
								<tbody id="table-body">
									<?php include('cars.php') ?>
								</tbody>
							</table>
						</li>
      					<li class="collection-item">
							<h5 class="center">Rental Duration</h5>
							<div class="input-field">
								<input id="rentdays" type="number" name="rentalDays" onkeyup="setRentalDays(this.value)"/>
								<label for="rentdays">Days</label>
							</div>
						</li>
      					<li class="collection-item">
							<h5 class="center">Preliminary Cost</h5>
							<h4 id="preliminaryRentalCost" class="center">$0.00</h4>
						</li>
						<li class="collection-item">
							<span class="error-text"><?php echo(@$error_text); ?></span>
							<button class="green darken-1 btn-flat white-text form-button" type="submit" name="submit"><strong>Rent Car</strong></button><br>
						</li>
					</form>
    			</ul>
			</div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="js/materialize.js"></script>
    <script src="js/init.js"></script>
	</body>
</html>