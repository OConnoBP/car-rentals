<?php session_start(); ?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
        <title>Enterprise - Rent a Car</title>
        <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    </head>
    <body>
        <?php include('navbar.php'); ?>
        <div class="container">
            <div class="card rental-form">
			    <div class="card-content">
                    <ul class="collection">
      					<li class="collection-item center-align">
                          <img class="responsive-img" src="images/logo.png"/>
                            <div class="carousel">
                                <a class="carousel-item"><img src="images/car_compact_2.png"></a>
                                <a class="carousel-item"><img src="images/car_luxury.png"></a>
                                <a class="carousel-item"><img src="images/car_standard.png"></a>
                                <a class="carousel-item"><img src="images/car_standard_2.png"></a>
                                <a class="carousel-item"><img src="images/car_compact.png"></a>
                                <a class="carousel-item"><img src="images/car_luxury_2.png"></a>
                            </div>
                            <h5 class="center-align">"Pick from our exclusive selection of vehicles"</h5>
                        </li>
                        <li class="collection-item">
                            <a href="rentalform.php">
                                <button class="green darken-1 btn-flat white-text form-button"><strong>Rent a Car</strong></button><br>
                            </a>
                        </li>
                    </ul>
			    </div>
		    </div>
        </div>
        <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="js/materialize.js"></script>
        <script src="js/init.js"></script>
    </body>
</html>