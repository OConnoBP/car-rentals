<div class="navbar-fixed">
    <nav>
      	<div class="nav-wrapper green darken-1">
			<a href="index.php" class="brand-logo center white-text">Enterprise</a>
			<a href="#" data-activates="mobile-demo" class="button-collapse white-text"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
            <?php
            if(@$_SESSION['username'] != null){
                echo "<li><a class='dropdown-button white-text' href='#!' data-activates='dropdown1'>Welcome, " . @$_SESSION['username'] . "<i class='material-icons right'>arrow_drop_down</i><i class='material-icons left white-text'>account_circle</i></a></li>";
                echo "<li><a class='white-text' href='logout.php'>Logout<i class='material-icons left white-text'>exit_to_app</i></a></li>";
            }
            else{
                echo "<li><a class='white-text' href='register.php'>Create an Account<i class='material-icons left white-text'>add_circle</i></a></li>";
                echo "<li><a class='white-text' href='login.php'>Login<i class='material-icons left white-text'>account_circle</i></a></li>";
            }
            ?>    
            </ul>
            <ul id="dropdown1" class="dropdown-content">
                <li><a href="rentedcars.php">Return a Rental</a></li>
                <li><a href="rentalform.php">Rent a Car</a></li>
            </ul>
      	</div>
    </nav>
</div>
<ul class="side-nav" id="mobile-demo">
	<?php
    if(@$_SESSION['username'] != null){
        echo "<li><a class='dropdown-button' href='#!' data-activates='dropdown1'>Welcome, " . @$_SESSION['username'] . "<i class='material-icons right'>arrow_drop_down</i></a></li>";
        echo "<li><a href='logout.php'>Logout</a></li>";
    }
    else{
        echo "<li><a class='black-text' href='login.php'>Login</a></li>";
        echo "<li><a class='black-text' href='register.php'>Create an Account</a></li>";
    }
    ?>
</ul>